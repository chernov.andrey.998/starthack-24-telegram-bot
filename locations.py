def get_office_locations(lang):
    return {
        "Switzerland": ["Basel", "Oensingen", "Zell", "Gossau", "Churwalden", "Cheseaux-sur-Lausanne", "Chermignon",
                        "Schlieren", "Genève", "Ohmstal", "Matzendorf", "Dällikon", "Dänikon"],
        "Germany": ["Barssel-Harkebrügge", "Edewecht", "Schiltach", "Seevetal", "Leinfelden-Echterdingen",
                    "Bad Wünnenberg", "Neuburg (Langenhaslach)", "Radolfzell"],
        "Spain": ["Casarrubios del Monte", "FUENSALIDA (Toledo)", "AZUAGA (Badajoz)", "Aguilas Murcia"],
        "Austria": ["Pfaffstätt", "Marchtrenk"],
        "Poland": ["NIEPOLOMICE", "Sopot", "Kostrzyn", "Łódź"],
        "France": ["Teilhede", "Riom", "Saint-Symphorien-sur-Coise", "Virieu-le-Grand", "Aime", "Valenton (Paris)"],
        "Belgium": ["Aalst"],
        "Liechtenstein": ["Schaan"],
        "Italy": ["Brivio"],
        "United Kingdom": ["Worcestershire"],
        "Czech Republic": ["Zásmuky u Kolína"],
        "Slovakia": ["Trnava"],
        "Hungary": ["Budapest"],
        "Romania": ["Pantelimon Ilfov"]
    }
