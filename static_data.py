greeting_choose_language = {
    'en': "Hello! Choose the language",
    'de': "Hallo! Wählen Sie die Sprache",
    'fr': "Bonjour ! Choisissez la langue"
}

question_name = {
    'en': 'Tell me about yourself. What is your name?',
    'de': 'Erzählen Sie mir von sich. Wie heißen Sie?',
    'fr': 'Parlez-moi de vous. Comment vous appelez-vous ?'
}

question_age = {
    'en': 'How old are you?',
    'de': 'Wie alt sind Sie?',
    'fr': 'Quel âge avez-vous ?'
}

question_nationality = {
    'en': 'What is your birth country? Please type answer in English',
    'de': 'Was ist Ihr Geburtsland? Bitte antworten Sie auf Englisch.',
    'fr': 'Quel est votre pays de naissance? Veuillez répondre en anglais.'
}

question_country = {
    'en': 'In which country is your office located?',
    'de': 'In welchem Land befindet sich Ihr Büro?',
    'fr': 'Dans quel pays se trouve votre bureau ?'
}

question_city = {
    'en': 'Got it! And which city?',
    'de': 'Verstanden! Und in welcher Stadt?',
    'fr': 'Compris ! Et dans quelle ville ?'
}

question_thanks_for_sharing_info = {
    'en': 'Thank you for sharing your information! What would you like to do next?',
    'de': 'Danke, dass Sie Ihre Informationen geteilt haben! Was möchten Sie als Nächstes tun?',
    'fr': 'Merci de partager vos informations ! Que souhaitez-vous faire ensuite ?'
}

question_let_me_know_event_finished = {
    'en': """Perfect! Enjoy :) Let me know, once event is finished""",
    'de': """Perfekt! Viel Spaß :) Lassen Sie mich wissen, wenn die Veranstaltung beendet ist""",
    'fr': """Parfait ! Profitez-en :) Faites-moi savoir, une fois que l'événement est terminé"""
}

keyboard_answer_company_questions = {
    'en': 'Answer Company Questions',
    'de': 'Unternehmensfragen beantworten',
    'fr': 'Répondre aux questions de l\'entreprise'
}

keyboard_participate_social_event = {
    'en': 'Participate in social event in your city',
    'de': 'Nehmen Sie an einer sozialen Veranstaltung in Ihrer Stadt teil',
    'fr': 'Participez à un événement social dans votre ville'
}

your_info = {
    'en': """Thank you for sharing your information! 

    Your name is {}
    Your age is {}
    Your nationality is {}
    Your country is {}
    Your city is {}

    What would you like to do next?""",
    'de': """Danke, dass Sie Ihre Informationen geteilt haben!

    Ihr Name ist {}
    Ihr Alter ist {}
    Ihre Staatsangehörigkeit ist {}
    Ihr Land ist {}
    Ihre Stadt ist {}

    Was möchten Sie als Nächstes tun?""",
    'fr': """Merci de partager vos informations !

    Votre nom est {}
    Votre âge est {}
    Votre nationalité est {}
    Votre pays est {}
    Votre ville est {}

    Que souhaitez-vous faire ensuite ?"""
}

answer_upcoming_event_location = {
    'en': """Great! 

In {} city the upcoming event: going to a bar
Date: 29.03.2024
Time: 18:00
Meeting point: office, ground floor

Are you willing to participate?""",
    'de': """Großartig!

In der Stadt {} das bevorstehende Ereignis: in eine Bar gehen
Datum: 29.03.2024
Zeit: 18:00
Treffpunkt: Büro, Erdgeschoss

Möchten Sie teilnehmen?""",
    'fr': """Super !

Dans la ville de {}, l'événement à venir : aller dans un bar
Date : 29.03.2024
Heure : 18:00
Point de rencontre : bureau, rez-de-chaussée

Souhaitez-vous participer ?"""
}

answer_decline_event = {
    'en': "I am sad to hear it :(. Maybe next time. If you have time, please tell me the reason, the reason why you are not going to participate. Anyway, I will inform you when the next event will take place",
    'de': "Es ist schade, das zu hören :(. Vielleicht beim nächsten Mal. Wenn du Zeit hast, nenne bitte den Grund, warum du nicht teilnehmen möchtest. Wie auch immer, ich werde dich informieren, wenn das nächste Ereignis stattfindet.",
    'fr': "Je suis triste de l'entendre :(. Peut-être la prochaine fois. Si vous avez le temps, veuillez indiquer la raison pour laquelle vous ne souhaitez pas participer. Quoi qu'il en soit, je vous informerai lorsque le prochain événement aura lieu."
}

answer_accept_event = {
    'en': "Cool! Press this button, once the event has finished",
    'de': "Cool! Drück diesen Knopf, sobald die Veranstaltung vorbei ist",
    'fr': "Cool ! Appuyez sur ce bouton, une fois l'événement terminé"
}

answer_feedback_event = {
    'en': "Good Job! Here is your badge :) I hope you enjoyed it! How was it? Could you please provide your feedback? \n Also think if you are willing to organise the next event in your city. If yes, just choose the place and inform me, I will send the information to your colleagues :)",
    'de': "Gut gemacht! Hier ist dein Abzeichen :) Ich hoffe, es hat dir gefallen! Wie war es? Könntest du bitte dein Feedback geben? \n Überlege auch, ob du bereit bist, das nächste Ereignis in deiner Stadt zu organisieren. Wenn ja, wähle einfach den Ort aus und informiere mich, ich werde die Informationen an deine Kollegen weiterleiten :)",
    'fr': "Bon travail! Voici votre badge :) J'espère que vous avez apprécié ! Comment était-ce ? \n Pourriez-vous donner votre retour, s'il vous plaît ? Pensez également si vous êtes prêt à organiser le prochain événement dans votre ville. Si oui, choisissez simplement le lieu et informez-moi, je transmettrai les informations à vos collègues :)"
}

answer_incorrect = {
    'en': "Oops! It is not the correct number",
    'de': "Hoppla! Das ist nicht die richtige Zahl",
    'fr': "Oups ! Ce n'est pas le bon nombre"
}

answer_correct = {
    'en': "Well done! {} is a correct answer. Here is your badge :)",
    'de': "Gut gemacht! {} ist eine richtige Antwort. Hier ist Ihr Abzeichen :)",
    'fr': "Bien joué ! {} est une réponse correcte. Voici votre badge :)"
}

question_country_quiz = {
    'en': "How many employees are from {}?",
    'de': "Wie viele Arbeitgeber kommen aus {}?",
    'fr': "Combien d'employeurs viennent de {} ?"
}

question_age_quiz = {
    'en': "How many employees are {} years old or younger by {}?",
    'de': "Wie viele Arbeitgeber sind {} Jahre alt oder jünger bis {}?",
    'fr': "Combien d'employeurs ont {} ans ou moins jusqu'à {} ?"
}

yes = {'en': 'Yes', 'de': 'Ja', 'fr': 'Oui'}
no = {'en': 'No', 'de': 'Nein', 'fr': 'Non'}
