import os
import random
from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import Application, CommandHandler, CallbackQueryHandler, MessageHandler, ConversationHandler, \
    filters, CallbackContext
from locations import get_office_locations
from static_data import *
import pandas as pd

# Define states
NAME, AGE, NATIONALITY, OFFICE_LOCATION_COUNTRY, OFFICE_LOCATION_CITY = range(5)


# lang = None


# Handler function for the "/start" command
async def choose_lang(update: Update, context: CallbackContext) -> None:
    keyboard = [
        [InlineKeyboardButton("English", callback_data='en')],
        [InlineKeyboardButton("German", callback_data='de')],
        [InlineKeyboardButton("French", callback_data='fr')]]
    reply_markup = InlineKeyboardMarkup(keyboard)
    await update.message.reply_text('Hello! Choose the language: ', reply_markup=reply_markup)


#
# async def tell_me(update: Update, context: CallbackContext) -> None:
#     query = update.callback_query
#     await query.answer()
#     # This example assumes "200" is the correct answer, adjust as needed
#     selected_answer = query.data  # Extracts the number from callback data like 'answer_100'
#     context.user_data['lang'] = selected_answer
#     keyboard = [[InlineKeyboardButton("Tell me about yourself", callback_data='tell_me_about_yourself')]]
#     reply_markup = InlineKeyboardMarkup(keyboard)
#     await query.message.reply_text('Got it, now tell me about yourself', reply_markup=reply_markup)


# CallbackQueryHandler function
async def tell_me_about_yourself(update: Update, context: CallbackContext) -> int:
    query = update.callback_query
    await query.answer()
    # This example assumes "200" is the correct answer, adjust as needed
    selected_answer = query.data  # Extracts the number from callback data like 'answer_100'
    context.user_data['lang'] = selected_answer
    lang = context.user_data['lang']
    await context.bot.send_message(chat_id=query.message.chat_id, text=question_name[lang])
    return NAME


# Handlers for collecting user information
async def name(update: Update, context: CallbackContext) -> int:
    context.user_data['name'] = update.message.text
    lang = context.user_data['lang']
    await update.message.reply_text(question_age[lang])
    return AGE


async def age(update: Update, context: CallbackContext) -> int:
    context.user_data['age'] = update.message.text
    lang = context.user_data['lang']
    await update.message.reply_text(question_nationality[lang])
    return NATIONALITY


async def nationality(update: Update, context: CallbackContext) -> int:
    context.user_data['nationality'] = update.message.text
    lang = context.user_data['lang']
    office_locations = get_office_locations(lang)
    office_locations_en = get_office_locations('en')
    keyboard = []
    for (k, k_en) in zip(office_locations.keys(), office_locations_en.keys()):
        keyboard.append([InlineKeyboardButton(k, callback_data=k_en)])
    reply_markup = InlineKeyboardMarkup(keyboard)
    await update.message.reply_text(question_country[lang],
                                    reply_markup=reply_markup)

    return OFFICE_LOCATION_COUNTRY


async def handle_country_answer(update: Update, context: CallbackContext) -> None:
    query = update.callback_query
    await query.answer()
    # This example assumes "200" is the correct answer, adjust as needed
    selected_answer = query.data  # Extracts the number from callback data like 'answer_100'
    context.user_data['country'] = selected_answer
    cities = get_office_locations('en')[selected_answer]
    keyboard = []
    for c in cities:
        keyboard.append([InlineKeyboardButton(c, callback_data=c)])
    reply_markup = InlineKeyboardMarkup(keyboard)
    lang = context.user_data['lang']
    await query.message.reply_text(question_city[lang],
                                   reply_markup=reply_markup)
    # return


async def handle_city_answer(update: Update, context: CallbackContext) -> None:
    query = update.callback_query
    await query.answer()
    # This example assumes "200" is the correct answer, adjust as needed
    selected_answer = query.data  # Extracts the number from callback data like 'answer_100'
    context.user_data['city'] = selected_answer
    lang = context.user_data['lang']
    keyboard = [
        [InlineKeyboardButton(keyboard_answer_company_questions[lang], callback_data='answer_questions')],
        [InlineKeyboardButton(keyboard_participate_social_event[lang], callback_data='participate_events')]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    lang = context.user_data['lang']
    response = question_thanks_for_sharing_info[lang].format(context.user_data['name'], context.user_data['age'],
                                                             context.user_data['nationality'],
                                                             context.user_data['country'],
                                                             context.user_data['city'])
    await query.message.reply_text(response, reply_markup=reply_markup)


async def answer_questions(update: Update, context: CallbackContext) -> None:
    query = update.callback_query
    await query.answer()

    df = pd.read_excel('employers.xlsx')
    random_number = random.random()
    try:  # in case the age has incorrect format
        int(context.user_data['age'])
    except ValueError:
        random_number = 0
    lang = context.user_data['lang']
    if random_number >= 0.5:
        random_number = random.randint(1, 10)
        question = question_age_quiz[lang].format(context.user_data['age'], random_number)
        answer = len(
            df.loc[df['Age'].between(int(context.user_data['age']) - random_number, int(context.user_data['age']))])
    else:
        question = question_country_quiz[lang].format(context.user_data['nationality'])
        answer = len(df.loc[df['Nationality'].str.lower() == context.user_data['nationality'].lower()])
    context.user_data['answer'] = answer
    context.user_data['question'] = question
    if answer == 0:
        candidate_1 = 1
        candidate_2 = 2
    else:
        candidate_1 = int(round(0.45 * answer))
        candidate_2 = int(round(1.55 * answer))
    keyboard = [
        [InlineKeyboardButton(str(candidate_1), callback_data='answer_false')],
        [InlineKeyboardButton(str(answer), callback_data='answer_true')],
        [InlineKeyboardButton(str(candidate_2), callback_data='answer_false')]
    ]
    random.shuffle(keyboard)
    reply_markup = InlineKeyboardMarkup(keyboard)
    await context.bot.send_message(chat_id=query.message.chat_id, text=question,
                                   reply_markup=reply_markup)


async def handle_answer(update: Update, context: CallbackContext) -> None:
    query = update.callback_query
    await query.answer()
    selected_answer = query.data  # Extracts the number from callback data like 'answer_100'
    lang = context.user_data['lang']
    if selected_answer == 'answer_true':
        response = answer_correct[lang].format(context.user_data['answer'])
        await context.bot.send_photo(chat_id=query.message.chat_id, photo="quiz.png",
                                     caption=response)

    else:
        response = answer_incorrect[lang]
        await context.bot.send_message(chat_id=query.message.chat_id, text=response)


async def participate_events(update: Update, context: CallbackContext) -> None:
    query = update.callback_query
    await query.answer()
    lang = context.user_data['lang']
    # This could link to a calendar, list events, or start another conversation
    keyboard = [
        [InlineKeyboardButton(yes[lang], callback_data='handle_participate_answer_yes')],
        [InlineKeyboardButton(no[lang], callback_data='handle_participate_answer_no')]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    response = answer_upcoming_event_location[lang].format(context.user_data['city'])
    await context.bot.send_message(chat_id=query.message.chat_id,
                                   text=response, reply_markup=reply_markup)


async def handle_participate_answer(update: Update, context: CallbackContext) -> None:
    query = update.callback_query
    await query.answer()
    # This example assumes "200" is the correct answer, adjust as needed

    selected_answer = query.data  # Extracts the number from callback data like 'answer_100'
    lang = context.user_data['lang']
    if selected_answer == "handle_participate_answer_yes":
        keyboard = [
            [InlineKeyboardButton(answer_accept_event[lang], callback_data='handle_feedback')]
        ]
        reply_markup = InlineKeyboardMarkup(keyboard)
        await context.bot.send_message(chat_id=query.message.chat_id,
                                       text=question_let_me_know_event_finished[lang],
                                       reply_markup=reply_markup)

    else:
        response = answer_decline_event[lang]
        await context.bot.send_message(chat_id=query.message.chat_id,
                                       text=response, )


async def handle_feedback(update: Update, context: CallbackContext) -> None:
    query = update.callback_query
    await query.answer()
    lang = context.user_data['lang']
    response = answer_feedback_event[lang]
    await context.bot.send_photo(chat_id=query.message.chat_id, photo="party.png",
                                 caption=response)


# Handler function for the "cancel" command
async def cancel(update: Update, context: CallbackContext) -> int:
    await update.message.reply_text('Operation cancelled.')
    return ConversationHandler.END


# Setup the conversation handler
conv_handler = ConversationHandler(
    entry_points=[CallbackQueryHandler(tell_me_about_yourself, pattern='^en$'),
                  CallbackQueryHandler(tell_me_about_yourself, pattern='^de$'),
                  CallbackQueryHandler(tell_me_about_yourself, pattern='^fr$')],
    states={
        NAME: [MessageHandler(filters.TEXT & ~filters.COMMAND, name)],
        AGE: [MessageHandler(filters.TEXT & ~filters.COMMAND, age)],
        NATIONALITY: [MessageHandler(filters.TEXT & ~filters.COMMAND, nationality)],
        OFFICE_LOCATION_COUNTRY: [MessageHandler(filters.TEXT & ~filters.COMMAND, handle_country_answer)],
        OFFICE_LOCATION_CITY: [MessageHandler(filters.TEXT & ~filters.COMMAND, handle_city_answer)],
    },
    fallbacks=[CommandHandler('cancel', cancel)],
)

# Creating the Application and registering handlers
TOKEN = os.getenv("TOKEN")
application = Application.builder().token(TOKEN).build()

application.add_handler(CommandHandler("start", choose_lang))
# application.add_handler(CommandHandler("tell_me", tell_me))
application.add_handler(conv_handler)
application.add_handler(CallbackQueryHandler(answer_questions, pattern='^answer_questions$'))
application.add_handler(CallbackQueryHandler(participate_events, pattern='^participate_events$'))
application.add_handler(CallbackQueryHandler(handle_participate_answer, pattern='^handle_participate_answer_yes$'))
application.add_handler(CallbackQueryHandler(handle_participate_answer, pattern='^handle_participate_answer_no$'))
application.add_handler(CallbackQueryHandler(handle_feedback, pattern='^handle_feedback'))

application.add_handler(CallbackQueryHandler(handle_answer, pattern='^answer_false$'))
application.add_handler(CallbackQueryHandler(handle_answer, pattern='^answer_true$'))

# application.add_handler(CallbackQueryHandler(tell_me_about_yourself, pattern='^en$'))
# application.add_handler(CallbackQueryHandler(tell_me_about_yourself, pattern='^de'))
# application.add_handler(CallbackQueryHandler(tell_me_about_yourself, pattern='^fr'))


offices = get_office_locations('en')

for c in offices.keys():
    application.add_handler(CallbackQueryHandler(handle_country_answer, pattern=f'^{c}$'))
    for cities in offices[c]:
        application.add_handler(CallbackQueryHandler(handle_city_answer, pattern=f'^{cities}$'))

# Start the bot
application.run_polling()
